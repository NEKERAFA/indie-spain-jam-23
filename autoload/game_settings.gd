extends Node
## Under GNU General Public License v3
## Copyright (C) 2023 - Rafael Alcalde Azpiazu (NEKERAFA)


const SAVE_PATH = "user://savedata.cfg"

const RESOLUTIONS = [
	Vector2i(480, 270), Vector2i(480, 272), Vector2i(640, 360), Vector2i(640, 480),
	Vector2i(720, 480), Vector2i(720, 540), Vector2i(800, 450), Vector2i(800, 600),
	Vector2i(960, 540), Vector2i(960, 544), Vector2i(1024, 576), Vector2i(1024, 768),
	Vector2i(1280, 720), Vector2i(1280, 800), Vector2i(1360, 768), Vector2i(1366, 768),
	Vector2i(1600, 900), Vector2i(1920, 1200), Vector2i(1920, 1080), Vector2i(2560, 1440),
]

var _save_file = ConfigFile.new()
var _modified = false

var resolution:
	get:
		return _save_file.get_value("Options", "resolution", RESOLUTIONS.find(Vector2i(1280, 720)))
	set(value):
		if resolution != value:
			_save_file.set_value("Options", "resolution", value)
			_modified = true

var fullscreen:
	get:
		return _save_file.get_value("Options", "fullscreen", false)
	set(value):
		if fullscreen != value:
			_save_file.set_value("Options", "fullscreen", value)
			_modified = true

var safe_mode:
	get:
		return _save_file.get_value("Options", "safe_mode", true)
	set(value):
		if safe_mode != value:
			_save_file.set_value("Options", "safe_mode", value)
			_modified = true


func save():
	if _modified and _save_file.save(SAVE_PATH) != OK:
		push_error("cannot save %s" % SAVE_PATH)
