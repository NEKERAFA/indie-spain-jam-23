class_name Constants

const VERSION = "1.0.0"

const GRAVITY = 1024.0
const MIN_GRAVITY = 2048.0
const MAX_GRAVITY = 8049.0
