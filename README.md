# ![Luna Lunática](misc/title.png)

Destrucción masiva al caer la noche. Juego realizado para la [Indie Spain Jam 2023](https://itch.io/jam/indie-spain-jam-23)

[![Descárgalo desde Itch.io](https://img.shields.io/badge/Itch.io-Descarga%20ahora-FF2449?logo=itchdotio&logoColor=white)](https://nekerafa.itch.io/luna-lunatica)
[![Hecho en Godot 4.1.1](https://img.shields.io/badge/Godot-4.1.1-blue?logo=godotengine&logoColor=white)](https://godotengine.org)
[![Bajo licencia GPLv3.0](https://img.shields.io/github/license/NEKERAFA/indie-spain-jam-23)](LICENSE)

## Sobre el juego

¿Te imaginas que estás tranquilamente en tu casa y de repente se te cae la luna? Espero que no, yo al menos ya tengo suficientes pesadillas con otras cosas como para añadir un nuevo miedo... ¿o tal vez no?

El objetivo de Luna Lunática es crear el caos cuando cae la noche, en este caso moviendo la luna de su órbita y haciendo que choque en objetos y aldeas arrededor del planeta. Ten cuidado con lo que te encuentras en órbita, o la audiencia nacional se cabreará.

Comentar que este juego fue hecho en directo en mi [canal de Twitch](https://twitch.tv/NEKERAFA), sin tener mucha idea de la parte de físicas, por lo que he aprendido un montón gracias a esta Jam.

## 🎮 Controles

Se recomienda jugar con ratón. Para lanzar la luna, mantén pulsado el botón principal de ratón y mueve el cursos por la pantalla para incrementar la fuerza del lanzamiento. Si liberas el botón del ratón, la luna saldrá disparada con el empujón que le has dado. ¡Prueba a lanzarla contra todos los elementos del escenario!

*Disclaimer*: si, metí contenido polémico, pero está desactivado por defecto. Para poder verlo tendrás que activarlo en el menú de configuración.

*Nota*: no me dio tiempo a poner música guay uwu.

## 🖥️ Obten el juego

Luna lunática está disponible en
[itch.io](https://nekerafa.itch.io/luna-lunatica) para las siguientes plataformas:

- GNU/Linux x86 64 bits
- Windows 64 bits
- macOS*

**Nota: la versión de macOS no está testeada aún. Puedes ver más información en [Running Godot apps on macOS](https://docs.godotengine.org/en/stable/tutorials/export/running_on_macos.html) (ENG) de la documentación de Godot.*

## Assets de terceros

Para este proyecto se han usado los siguientes assets:

- [Creato Display](https://www.dafont.com/creato-display.font): OFL 1.1 - Anugrah Pasau.s
- [Sniglet](https://fonts.google.com/specimen/Sniglet): OFL 1.1 - Haley Fiege, Brenda Gallo, Pablo Impallari

# Licencia

Los assets están bajo licencia [Creative Commons Attribution-ShareAlike Version 4](https://creativecommons.org/licenses/by-sa/4.0/) (CC BY-SA 4.0). Siéntete libre de mirar como funciona, modificarlo, y exportarlo para tu plataforma siempre y cuando se respete la licencia anterior.

> GNU General Public License Version 3, 29 June 2007
>
> Copyright (c) 2023 Rafael Alcalde Azpiazu (NEKERAFA)
>
> You may copy, distribute and modify the software as long as you track
> changes/dates in source files. Any modifications to or software including (via
> compiler) GPL-licensed code must also be made available under the GPL along
> with build & install instructions.
>
> See all legal notice in LICENSE file
