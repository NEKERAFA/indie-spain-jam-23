extends Node
## Under GNU General Public License v3
## Copyright (C) 2023 - Rafael Alcalde Azpiazu (NEKERAFA)


func _input(event):
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT and $Moon.freeze:
		$Moon._launch(_get_impulse())
		$CameraFollower.follow_to = $Moon
	if event is InputEventKey and event.keycode == KEY_R:
		get_tree().reload_current_scene()


func _process(_delta):
	if $Moon.freeze:
		$Line2D.points[0] = $Moon.global_position
		$Line2D.points[1] = get_viewport().get_mouse_position()
		
		var impulse = _get_impulse()
		$Breadcrumbs.update_breadcrumbs(impulse)
		$CameraFollower/ImpulseVector.vector = impulse
		
	var direction = $Earth.global_position - $Moon.global_position
	var force = min(max(direction.length(), 2048.0), 8192.0)
	var gravity = direction.normalized() * Constants.GRAVITY * force / 2048.0
	$CameraFollower/GravityVector.vector = gravity

	$CameraFollower/Label.text = "Moon: (%.2f, %.2f)\nGravity: %.2f (%2.f, %.2f)" % [
		$Moon.global_position.x,
		$Moon.global_position.y,
		Constants.GRAVITY * force / 2048.0,
		gravity.x,
		gravity.y
	]

func _get_impulse():
	var impulse = ($Moon.global_position - get_viewport().get_mouse_position())
	var force = min(impulse.length(), 200.0) / 200
	return impulse.normalized() * (force * force) * 4000
