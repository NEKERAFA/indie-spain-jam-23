extends Node
## Under GNU General Public License v3
## Copyright (C) 2023 - Rafael Alcalde Azpiazu (NEKERAFA)


@onready var earth = $Earth
@onready var moon = $Moon
@onready var moon_ghost = $MoonGhost
@onready var impulse_line = $Impulse
@onready var breadcrumbs = $Breadcrumbs
@onready var camera = $CameraFollower
@onready var timer_lbl = $CameraFollower/TimerLabel
@onready var points_lbl = $CameraFollower/PointsLabel
@onready var achieves = $CameraFollower/Achieves
@onready var timer = $Timer
@onready var pause_menu = $CameraFollower/PauseMenu
@onready var settings_menu = $CameraFollower/SettingsMenu
@onready var tutorial = $CameraFollower/Tutorial
@onready var gameover = $CameraFollower/GameoverMenu
@onready var final_points = $CameraFollower/GameoverMenu/Points


var points = 0


func _input(event):
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT:
		tutorial.hide()

		if moon.freeze:
			moon_ghost.visible = event.pressed

			if not event.pressed:
				impulse_line.set_point_position(0, Vector2.ZERO)
				impulse_line.set_point_position(1, Vector2.ZERO)
				breadcrumbs.clear_points()
				
				moon._launch(_get_impulse())
				camera.follow_to = moon

	if event is InputEventKey and event.keycode == KEY_ESCAPE:
		get_tree().paused = true
		pause_menu.show()


func _process(delta):
	if Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT) and moon.freeze:
		var direction = (camera.get_global_mouse_position() - moon.position)
		var force = min(direction.length(), 200.0)
	
		moon_ghost.position = moon.position + direction.normalized() * force
		moon_ghost.scale = Vector2.ONE * force / 200.0 * 0.15
		impulse_line.set_point_position(0, moon.position + direction.normalized() * force)
		impulse_line.set_point_position(1, moon.position)
		breadcrumbs.update_breadcrumbs(_get_impulse())

	timer_lbl.text = "%d" % round(timer.time_left)

func _get_impulse():
	var direction = (moon.position - camera.get_global_mouse_position())
	var force = min(direction.length(), 200.0) / 200
	return direction.normalized() * (force * force) * 4000


func _on_timer_timeout():
	get_tree().paused = true
	gameover.show()
	final_points.text = str(points)


func _on_village_destroyed():
	achieves.add_achieve("¡Aldea destruida!", 10)
	points += 10
	points_lbl.text = str(points)


func _on_village_removed():
	achieves.add_achieve("¡Has arrasado con la aldea!", 200)
	points += 200
	points_lbl.text = str(points)


func _on_volcano_eruptioned():
	achieves.add_achieve("¡Volcan en erupción!", 200)
	points += 200
	points_lbl.text = str(points)


func _on_moon_sleeping_state_changed():
	if moon.sleeping:
		moon.freeze = true
		camera.follow_to = null
		var direction = earth.position.direction_to(moon.position)
		camera.position = earth.position + direction * 1200


func _on_satellite_collided():
	achieves.add_achieve("¡Satélite alcanzado!", 100)
	points += 100
	points_lbl.text = str(points)


func _on_space_car_easter_egg():
	achieves.add_achieve("¡Audiencia Nacional Intensifies!", 1000)
	points += 1000
	points_lbl.text = str(points)


func _on_roadstered():
	achieves.add_achieve("¡Roadstered!", 1000)
	points += 1000
	points_lbl.text = str(points)


func _on_back_button_pressed():
	pause_menu.show()
	settings_menu.hide()


func _on_pause_menu_show_options():
	pause_menu.hide()
	settings_menu.show()


func _on_restart_pressed():
	get_tree().paused = false
	get_tree().reload_current_scene()


func _on_main_menu_button_down():
	get_tree().paused = false
	get_tree().change_scene_to_file("res://scenes/main_scene.tscn")
