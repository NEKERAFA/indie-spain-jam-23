extends Control
## Under GNU General Public License v3
## Copyright (C) 2023 - Rafael Alcalde Azpiazu (NEKERAFA)


const ANIMATION = 0.25


func _ready():
	$GameInfo/Version.text = "Versión %s" % Constants.VERSION


func _on_main_menu_play_button_pressed():
	get_tree().change_scene_to_file("res://scenes/game.tscn")


func _on_main_menu_options_button_pressed():
	var tween = create_tween().set_parallel(true).set_trans(Tween.TRANS_QUAD)
	tween.tween_property($MainMenu, "anchor_left", -1, ANIMATION)
	tween.tween_property($MainMenu, "anchor_right", 0, ANIMATION)
	tween.tween_property($Settings, "anchor_left", 0, ANIMATION)
	tween.tween_property($Settings, "anchor_right", 1, ANIMATION)


func _on_settings_back_button_pressed():
	var tween = create_tween().set_parallel(true).set_trans(Tween.TRANS_QUAD)
	tween.tween_property($MainMenu, "anchor_left", 0, ANIMATION)
	tween.tween_property($MainMenu, "anchor_right", 1, ANIMATION)
	tween.tween_property($Settings, "anchor_left", 1, ANIMATION)
	tween.tween_property($Settings, "anchor_right", 2, ANIMATION)
