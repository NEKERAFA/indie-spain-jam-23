extends Node2D
## Under GNU General Public License v3
## Copyright (C) 2023 - Rafael Alcalde Azpiazu (NEKERAFA)


@export var vector = Vector2.ZERO
@export var color = Color.CYAN
@export var reference = Vector2.UP


@onready var magnitude_lbl = $Label
@onready var vector_gizmo = $Gizmo


func _ready():
	vector_gizmo.default_color = color


func _process(delta):
	magnitude_lbl.text = "%.2f" % vector.length()
	vector_gizmo.rotation = reference.angle_to(vector)
	magnitude_lbl.position.y = -26 -33 * cos(max(0, reference.angle_to(vector)))
