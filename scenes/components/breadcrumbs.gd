extends Line2D
## Under GNU General Public License v3
## Copyright (C) 2023 - Rafael Alcalde Azpiazu (NEKERAFA)


@export var _moon: Node2D
@export var _earth: Node2D


var _delta_time = 1.0/120.0
var _impulse = Vector2.ZERO


func update_breadcrumbs(impulse):
	_impulse = impulse
	_calculate_breadcrumbs()


func _calculate_breadcrumbs():
	clear_points()
	
	add_point(_moon.global_position)
	var velocity = _impulse
	var i = 1

	while i > 0 and i < 480:
		var last_position = get_point_position(i - 1)
		
		var direction = _earth.global_position - last_position
		var force = min(max(direction.length(), Constants.MIN_GRAVITY), Constants.MAX_GRAVITY)
		var gravity = direction.normalized() * Constants.GRAVITY * force / Constants.MIN_GRAVITY
		
		velocity += gravity * _delta_time
		var next_point = last_position + velocity * _delta_time
		
		if (_earth.global_position - next_point).length() < _earth.get_radius() + _moon.get_radius() / 2:
			i = -1
		else:
			add_point(next_point)
			i += 1
