extends Camera2D
## Under GNU General Public License v3
## Copyright (C) 2023 - Rafael Alcalde Azpiazu (NEKERAFA)


@export var earth: Node2D
@export var follow_to: Node2D


func _physics_process(_delta):
	if follow_to != null:
		var gravity_vector = (earth.global_position - follow_to.global_position).normalized()
		global_position = follow_to.global_position
		rotation = Vector2.DOWN.angle_to(gravity_vector)
