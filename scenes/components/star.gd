extends Sprite2D


signal screen_exited(node)


var star_textures = [
	preload("res://assets/sprites/star.png"), preload("res://assets/sprites/star-big.png")
]

var colors = [
	Color.WHITE, Color.AQUAMARINE, Color.CORAL, Color.GOLD, Color.CORNFLOWER_BLUE, Color.MEDIUM_PURPLE, Color.PALE_TURQUOISE
]


var random = RandomNumberGenerator.new()
var is_big = false
var color = Color.TRANSPARENT
var tween


func _ready():
	random.randomize()
	show_star()


func show_star():
	if tween:
		tween.kill()
	
	is_big = random.randf() > 0.5
	color = colors[randi() % colors.size()]

	texture = star_textures[1 if is_big else 0]
	modulate = Color(color, 0)
	scale = Vector2.ONE * randf_range(0.05, 0.2)
	if is_big:
		scale *= 0.8

	tween = create_tween().set_parallel(true)
	tween.tween_property(self, "modulate", color, 0.25)
	tween.tween_callback(_on_tween_finished).set_delay(randf_range(1.0, 5.0))


func _on_tween_finished():
	var tween = create_tween().set_parallel(true)
	tween.tween_property(self, "modulate", Color(color, 0), 0.25)
	tween.tween_callback(_on_screen_exited).set_delay(0.25)


func _on_screen_exited():
	screen_exited.emit(self)
