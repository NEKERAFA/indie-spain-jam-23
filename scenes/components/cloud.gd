extends Sprite2D


@export
var angular_velocity: float


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotate(angular_velocity * delta)
