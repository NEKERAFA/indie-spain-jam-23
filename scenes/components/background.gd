extends Node2D


@export var elements = 16
@export var camera: Node2D

var star = preload("res://scenes/components/star.tscn")
var random = RandomNumberGenerator.new()


func _ready():
	random.randomize()

	for i in range(elements):
		var node = star.instantiate()
		add_child(node)
		
		node.screen_exited.connect(self._on_screen_exited)
		node.position = (camera.global_position if camera else Vector2(480, 270)) + Vector2(
			random.randi_range(-640, 640),
			random.randi_range(-640, 640)
		)


func _on_screen_exited(node):
	node.position = (camera.global_position if camera else Vector2(480, 270)) + Vector2(
		random.randi_range(-640, 640),
		random.randi_range(-640, 640)
	)
	
	node.show_star()
