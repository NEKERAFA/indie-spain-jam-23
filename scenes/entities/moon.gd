extends RigidBody2D
## Under GNU General Public License v3
## Copyright (C) 2023 - Rafael Alcalde Azpiazu (NEKERAFA)


@export var earth: Node2D


var gravity = Vector2()


func get_radius():
	return ($CollisionShape2D.shape as CircleShape2D).radius


func _process(delta):
	var angle = Vector2.DOWN.angle_to(gravity)
	$Sprite2D.rotation = angle


func _launch(impulse):
	if freeze:
		freeze = false
		apply_impulse(impulse)


func _integrate_forces(state):
	var direction = earth.global_position - global_position
	var force = min(max(direction.length(), 2048.0), 8192.0)
	gravity = direction.normalized() * Constants.GRAVITY * force / 2048.0
	state.apply_central_force(gravity)


func _on_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
	if linear_velocity.length() > 200 and "player_collided" in body:
		body.player_collided()
