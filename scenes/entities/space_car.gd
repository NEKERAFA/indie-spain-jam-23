extends RigidBody2D


signal easter_egg
signal roadstered


func _ready():
	if not GameSettings.safe_mode:
		$CarSafe.disabled = true
		$CarUnsafe.disabled = false
		$Sprite2D.texture = load("res://assets/sprites/carrero-blanco.png")


func player_collided():
	if GameSettings.safe_mode:
		roadstered.emit()
	else:
		easter_egg.emit()
