extends StaticBody2D
## Under GNU General Public License v3
## Copyright (C) 2023 - Rafael Alcalde Azpiazu (NEKERAFA)


func get_radius():
	return ($CollisionShape2D.shape as CircleShape2D).radius


func _process(delta):
	var distance = get_viewport().get_camera_2d().global_position - global_position
	var angle = Vector2.UP.angle_to(distance)
	$Sprite2D.rotation = angle
