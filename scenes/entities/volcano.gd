extends StaticBody2D
## Under GNU General Public License v3
## Copyright (C) 2023 - Rafael Alcalde Azpiazu (NEKERAFA)


signal volcano_eruptioned


const MAX_SHAKE_TIME = 3

enum State { IDLE, SHAKING, BROKEN }

var _current_state = State.IDLE
var _elapsed_time = 0
var _initial_position


func _input(event):
	if OS.is_debug_build():
		if event is InputEventKey and event.keycode == KEY_S:
			player_collided()


func player_collided():
	if _current_state == State.IDLE:
		_current_state = State.SHAKING
		_initial_position = position
		$Sprite.texture = load("res://assets/sprites/volcano-broken.png")
		$FogParticles.emitting = true
		$LavaParticles.emitting = true
		volcano_eruptioned.emit()


func _process(delta):
	if _current_state == State.SHAKING:
		_elapsed_time += delta
		if _elapsed_time >= MAX_SHAKE_TIME:
			position = _initial_position
			_current_state = State.BROKEN
		else:
			position = _initial_position + transform.basis_xform(Vector2.LEFT) * shake_movement(_elapsed_time) * shake_intensity(_elapsed_time)


func shake_movement(time):
	return cos(time * 10 * PI)


func shake_intensity(time):
	return (MAX_SHAKE_TIME - time) * (MAX_SHAKE_TIME - time) * 10
