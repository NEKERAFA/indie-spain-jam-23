extends Area2D
## Under GNU General Public License v3
## Copyright (C) 2023 - Rafael Alcalde Azpiazu (NEKERAFA)


signal village_destroyed
signal village_removed


var destroyed = false
var removed = false
var collisions = 0


func _on_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
	if body.name == "Moon":
		collisions += 1

		if not destroyed:
			$Sprite.texture = load("res://assets/sprites/village-destroyed.png")
			$FogParticles.emitting = true
			$EmberParticles.emitting = true
			destroyed = true
			village_destroyed.emit()
		elif collisions >= 10 and removed:
			$FogParticles.emitting = false
			$EmberParticles.emitting = false
			village_removed.emit()
