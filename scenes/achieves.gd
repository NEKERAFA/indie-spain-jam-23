extends VBoxContainer


var achieve_label = preload("res://ui/achieve.tscn")


func add_achieve(achieve, points):
	var node = achieve_label.instantiate()
	add_child(node)
	node.text = "{0} (+{1})".format([achieve, points])
