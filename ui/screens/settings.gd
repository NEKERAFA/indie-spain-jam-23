extends Control
## Under GNU General Public License v3
## Copyright (C) 2023 - Rafael Alcalde Azpiazu (NEKERAFA)


signal back_button_pressed


@onready var _resolution_btn = $Menu/ResolutionContainer/OptionButton
@onready var _fullscreen_check = $Menu/FullscreenContainer/FullscreenCheck
@onready var _safemode_check = $Menu/SafeModeContainer/SafeModeCheck


func _ready():
	for resolution in GameSettings.RESOLUTIONS:
		_resolution_btn.add_item("{0}x{1}".format([resolution.x, resolution.y]))
	if GameSettings.fullscreen:
		_resolution_btn.disabled = true
	else:
		_resolution_btn.select(GameSettings.resolution)
		_resolution_btn.call_deferred("grab_focus")

	_fullscreen_check.button_pressed = GameSettings.fullscreen
	_safemode_check.button_pressed = GameSettings.safe_mode
	_safemode_check.call_deferred("release_focus")


func _input(event):
	if event is InputEventKey and event.keycode == KEY_ESCAPE:
		_on_back_button_pressed()


func _on_option_button_item_selected(index):
	GameSettings.resolution = index
	get_tree().root.size = GameSettings.RESOLUTIONS[index]


func _on_fullscreen_check_toggled(button_pressed):
	GameSettings.fullscreen = button_pressed
	get_tree().root.mode = Window.MODE_FULLSCREEN if button_pressed else Window.MODE_WINDOWED
	_resolution_btn.disabled = button_pressed


func _on_safe_mode_check_toggled(button_pressed):
	GameSettings.safe_mode = button_pressed


func _on_back_button_pressed():
	back_button_pressed.emit()
	GameSettings.save()


func _on_credits_button_pressed():
	$LicensePopup.show()
