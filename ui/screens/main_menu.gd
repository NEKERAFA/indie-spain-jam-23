extends Control
## Under GNU General Public License v3
## Copyright (C) 2023 - Rafael Alcalde Azpiazu (NEKERAFA)


signal play_button_pressed
signal options_button_pressed


func _ready():
	$AnimationPlayer.play("show_title")


func _on_animation_player_animation_finished(anim_name):
	if anim_name == "show_title":
		$Menu.show()
		$Menu/PlayButton.grab_focus()


func _on_play_button_pressed():
	play_button_pressed.emit()


func _on_options_button_pressed():
	options_button_pressed.emit()


func _on_quit_pressed():
	get_tree().quit(0)
