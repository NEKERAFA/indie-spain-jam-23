extends ColorRect


signal show_options


func _input(event):
	if event is InputEventKey and event.keycode == KEY_ESCAPE:
		_on_continue_pressed()


func _on_continue_pressed():
	get_tree().paused = false
	hide()


func _on_main_menu_pressed():
	get_tree().paused = false
	get_tree().change_scene_to_file("res://scenes/main_scene.tscn")


func _on_options_pressed():
	show_options.emit()


func _on_exit_pressed():
	get_tree().paused = false
	get_tree().quit(0)
