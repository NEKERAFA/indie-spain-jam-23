extends Label

func _ready():
	modulate = Color.TRANSPARENT
	var tween = create_tween().set_parallel(true).set_trans(Tween.TRANS_QUAD)
	tween.tween_property(self, "modulate", Color.WHITE, 0.2)
	tween.tween_callback($Timer.start).set_delay(0.2)


func _on_timer_timeout():
	call_deferred("queue_free")
