extends ColorRect
## Under GNU General Public License v3
## Copyright (C) 2023 - Rafael Alcalde Azpiazu (NEKERAFA)


func _on_button_pressed():
	hide()


func _on_rich_text_label_meta_clicked(meta):
	OS.shell_open(meta)
